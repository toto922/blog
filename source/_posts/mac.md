---
title: mac
date: 2022-10-09 18:28:13
categories: [IT]
tags: [mac]
---

⌘⌥⌃⇧⌫⌦

## 初始化

### script for 2k monitor
``` bash
$ sh -c "$(curl -fsSL https://raw.githubusercontent.com/xzhih/one-key-hidpi/master/hidpi.sh)"
```

### 以root权限打开vscode
- 打开命令面板（在Mac上为⌘+ ⇧+ P）或查看 ❯ 命令面板
- 键入shell command查找 Shell Command: Install ‘code’ command in PATH command

## 快捷键

### 强制退出
- <kbd>⌘</kbd> + <kbd>⌥</kbd> + <kbd>esc</kbd>

### 截图
- <kbd>⌘</kbd> + <kbd>⇧</kbd> + <kbd>5</kbd>
