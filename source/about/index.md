---
title: 关于
date: 2022-09-30 13:45:29
type: "about"
---

| Key | Value |
| :-| :- | 
| Gitlab | [https://gitlab.com/toto922/blog](https://gitlab.com/toto922/blog) |
| Easy Hexo | [https://easyhexo.com/](https://easyhexo.com/) |
| Hexo Doc | [https://hexo.io/zh-cn/](https://hexo.io/zh-cn/) |
| Hexo Theme | [https://github.com/next-theme/hexo-theme-next](https://github.com/next-theme/hexo-theme-next) |
