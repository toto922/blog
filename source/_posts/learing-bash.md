---
title: 如何用nohup与&、在bash的后台运行命令
date: 2022-02-12 16:59:04
updated: 2022-02-18 16:59:04
categories: [IT]
tags: [linux, bash]
---

## matrix
|                         | <kbd>Ctrl</kbd> + <kbd>C</kbd> (SIGINT) | 关闭session(SIGHUP) |
| :---------------------- | --------------------------------------- | ------------------- |
| command                 | 关闭                                    | 关闭                |
| **nohup** command       | 关闭                                    | -                   |
| command **&**           | -                                       | 关闭                |
| **nohup** command **&** | -                                       | -                   |

## 如何关闭后台的程序
```bash
$ ps -ef
$ kill PID
```
