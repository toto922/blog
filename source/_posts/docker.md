---
title: Docker命令整理
date: 2022-09-30 09:00:37
categories: [IT]
tags: [linux, docker]
---

> 学习《第一本DOCKER书》
> 
>  [https://book.douban.com/subject/26780404/](https://book.douban.com/subject/26780404/)

## Container Commands

### get infomation
``` bash
# docker infomation
$ sudo docker info
# container infomation
$ sudo docker inspect bob_the_container
```

### container run
``` bash
$ sudo docker run -i -t ubuntu /bin/bash
$ sudo hostname
$ sudo cat /etc/hosts
$ sudo ip a
$ sudo ps -aux
$ sudo apt-get update && apt-get install vim
$ exit
```

### list all containers
``` bash
$ sudo docker ps -a
```

### run container in customize name
``` bash
$ sudo docker run --name bob_the_container -i -t ubuntu /bin/bash
```

### start and stop container by name or hash
``` bash
# by name
$ sudo docker start bob_the_container
$ sudo docker stop bob_the_container

# by hash
$ sudo docker start aa3f365fofbee
$ sudo docker stop aa3f365fofbee
$ sudo docker rm aa3f365fofbee
```

### attach container by name or hash
``` bash
# by name
$ sudo docker attach bob_the_container

# by hash
$ sudo docker attach aa3f365fofbee
```

### run long term container -d (daemon process)
``` bash
$ sudo docker run --name daemon_dave -d ubuntu /bin/sh -c "while true; do echo hello world; sleep 1; done"

# --restart automatically restart the container when exit
$ sudo docker run --restart=always --name daemon_dave -d ubuntu /bin/sh -c "while true; do echo hello world; sleep 1; done"

# show logs
$ sudo docker logs daemon_dave

# list process in running docker
$ sudo docker top daemon_dave

# show statistics
$ sudo docker stats daemon_dave
```

### exec
``` bash
# exec: create a new process for bash. exit will not stop the container
# attach: get inside PID 1 of a container. exit will stop the container
$ sudo docker exec -t -i daemon_dave /bin/bash

# run task in back
$ sudo docker exec -d daemon_dave touch /etc/new_config_file

# with -d, result not show. without -d, result show
$ sudo docker exec -d daemon_dave date
$ sudo docker exec daemon_dave date
```
## Image Commands

### list all images
``` bash
$ sudo docker images
```

### pull image
``` bash
$ sudo docker pull ubuntu:12.04
```

