---
title: nodejs & typescript
date: 2022-11-18 10:51:23
categories: [IT]
tags: [nodejs, typescript]
---

## nodejs

### 再帰

```javascript
const getSelectAllIds = async (url) => {
  const response = await axios.post(url);
  if (!response.ok) throw new Error("HTTP error"); // lets not ignore these.
  return [
    ...response.data.results,
    ...(response.data.next ? await getSelectAllIds(response.data.next) : []),
  ];
};
```

### console.log

```javascript
# hoge
console.log('%o', parsed.html)
```

### async

setTimeout using await and promise

```javascript
await new Promise((resolve) => setTimeout(resolve, 4000));
```

### stream

读，写

```javascript
const fs = require("fs");

// normal write will crash
function writeNormal() {
  const data = ["name,cost"];
  for (let i = 0; i < 1e8; i++) {
    data.push(`${i},1`);
  }
  fs.writeFileSync("output.csv", data.join("\n"));
}

// stream write will not crash
async function writeSteam() {
  const writeStream = fs.createWriteStream("output.csv");
  for (let i = 0; i < 1e7; i++) {
    const overWatermark = writeStream.write(`${i},1\n`);
    if (!overWatermark) {
      await new Promise((resolve) => writeStream.once("drain", resolve));
    }
  }
  writeStream.end();
}

// normal read
function readNormal() {
  var data = fs.readFileSync("./output.csv");
  console.log(data);
}

// stream read
function readStream() {
  var stream = fs.createReadStream("./output.csv");

  stream.on("open", (data) => {
    console.log("open");
  });

  stream.on("data", (data) => {
    console.log(data.toString());
  });

  stream.on("end", (data) => {
    console.log("end");
  });
}
```

## typescript

### 定义 Object 类型

```javascript
type GenericObject = { [key: string]: any };
```
