---
title: 快捷键
date: 2023-11-17 9:11:24
categories: [IT]
tags: [tool]
---

# Chrome

| 快捷键         | 操作             |
| :------------- | :--------------- |
| Ctrl + N       | 打开一个新窗口   |
| Ctrl + T       | 打开一个新标签页 |
| Ctrl + L 或 F6 | 跳到地址栏       |
