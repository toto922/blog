---
title: python
date: 2024-02-13 17:28:31
categories: [IT]
tags: [python]
---

## python

### set up environment and create requirements.txt

```bash
# Create a virtual environment
python -m venv myenv

# Activate the virtual environment
myenv\Scripts\activate

# If activate failed
Set-ExecutionPolicy Unrestricted -Force

# Install Dependencies
pip install pandas

# Generate the requirements.txt File
pip freeze > requirements.txt

# deactivate
deactivate
```
