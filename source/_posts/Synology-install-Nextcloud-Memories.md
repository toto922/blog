---
title: Synology install Nextcloud Memories
date: 2023-09-15 18:27:15
categories: [IT]
tags: [nas]
---

# step 1

docker compose install with portainer

link: https://www.youtube.com/watch?v=V1tA858WiKw&ab_channel=WunderTech

# step 2

install Nextcloud Memories plugin within Nextcloud store

# step 3

install external storage plugin within Nextcloud store and mount with local folder

# step 4

install ffmpeg and enable image and video preview

```json
...
2 => 'OC\\Preview\\JPEG',
...
```

```bash
apt-get update && apt-get install -y ffmpeg
```

# tips 1

when can not write config.php

```bash
chown -R www-data:www-data config.php
```

# tips 2

how to index Nextcloud Memories with cli like terminal

```bash
sudo docker exec -u 33 nextcloud-app-1 php occ memories:index
```
